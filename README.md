# Screenshots of [Packuru](https://gitlab.com/kyeastmood/packuru)

## Desktop

### Overview
![Overview](desktop/overview.png)

### Browser
![Browser](desktop/browser.png)
![Archive properties](desktop/browser-properties.png)
![Encrypted archive](desktop/browser-encrypted.png)
![Invalid password](desktop/browser-invalid-password.png)
![Ruined archive](desktop/browser-ruined.png)
![In progress](desktop/browser-in-progress.png)
![Delete files](desktop/browser-delete.png)

### Queue
![Queue](desktop/queue.png)
![Create](desktop/create-new-1-files.png)
![Create](desktop/create-new-2-archive.png)
![Create](desktop/create-new-3-options.png)
![Create](desktop/create-new-4-advanced.png)
![Extract](desktop/extract-1-archives.png)
![Extract](desktop/extract-2-options.png)

### Settings
![General](desktop/settings-general.png)
![Browser](desktop/settings-browser.png)
![Queue](desktop/settings-queue.png)
![Plugins](desktop/settings-plugins.png)
![Types](desktop/settings-archive-types.png)

### Integration
![Integration](desktop/integration.png)

## Mobile

### Overview
![Overview](mobile/overview.png)

### Browser
![Browser](mobile/browser.png)
![Global Menu](mobile/browser-global-menu.png)
![Sorting](mobile/browser-sorting.png)
![Archive properties](mobile/browser-properties.png)
![Encrypted archive](mobile/browser-encrypted.png)
![Invalid password](mobile/browser-invalid-password.png)
![Ruined archive](mobile/browser-ruined.png)
![In progress](mobile/browser-in-progress.png)
![Delete files](mobile/browser-delete.png)

### Queue
![Queue](mobile/queue.png)
![Create](mobile/create-new-1-files.png)
![Create](mobile/create-new-2-archive.png)
![Create](mobile/create-new-3-options.png)
![Create](mobile/create-new-4-advanced.png)
![Extract](mobile/extract-1-archives.png)
![Extract](mobile/extract-2-options.png)

### Settings
![Settings](mobile/settings.png)
![General](mobile/settings-general.png)
![Browser](mobile/settings-browser.png)
![Queue](mobile/settings-queue.png)
![Plugins](mobile/settings-plugin-7zip.png)
![Types](mobile/settings-archive-types.png)
